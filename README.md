# <od-wizard>

> A component to quickly create a step-by-step wizard  

`<od-wizard>` is a simple component that allows the user to define a set of pages to be displayed in an orderly fashion. The wizard allows users to progress to the first incomplete step and backtrack to any previous step. It provides a callback hook when the user has completed the wizard.

`<od-wizard-page>` is a simple component that allows the user to define a page name for od-wizard steps. It provides  callback hooks for when the user loads the page andd when the user advances the page.

## Installation
- Install with [npm](https://www.npmjs.com/)

```
npm i @orcden/od-wizard
```
## Usage
```
import '@orcden/od-wizard';
```
```
<od-wizard>
    <label slot='title'>Example Wizard</label>
    <od-wizard-page title='Step 1'>
        <p>Step 1</p>    
    </od-wizard-page>
    <od-wizard-page title='Step 2'>
        <p>Step 2</p>
    </od-wizard-page>
</od-wizard>
```
    
## Attributes
### OD-Wizard-Page
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `title`  | String  | undefined      | *Required* This is the title that the wizard will use to generate the navigation item  |

## Properties
### OD-Wizard
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `doneCallback`  | function  | undefined   | Params: od-wizard - this function will be called once the final step of the wizard returns true for its callback  |

### OD-Wizard-Page
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `loadCallback`  | function  | undefined   | Params: od-wizard-page - this function will be called when the page is loaded  |
| `doneCallback`  | function  | undefined   | Params: od-wizard-page - this function will be called when the user hits the next button. It must return true or false to indicate to the wizard if it is allowed to progress to the next step.  |

## Functions
### OD-Wizard-Page
| Name | Parameters | Description                                  |
|-----------|------|-----------------------------------------------|
| `executeLoad`   | None | Manually call the load function of the page with its scope pased through |
| `executeDone`   | None | Manually call the done function of the page with its scope pased through |

## Styling
- CSS variables are available to alter the default styling provided

### OD-Wizard
| Shadow Parts     | Description           |
|------------------|-----------------------|
| title-slot   | The slot used to hold the Title item |
| nav-container    | The div used to hold the nav and the pages container |
| nav         | The actual navigation list |
| nav-item         | The individual "links" in the navigation |
| nav-item-enabled         | The enabled links in the navigation |
| nav-item-current         | The current link/step in the navigation |
| pages-container  | The div the holds the individual pages and gthe buttons container |
| buttons-container | The div that holds the control buttons |
| prev-button          | The Back Button |
| next-button          | The Next Button |
| finish-button          | The Finish Button |

## Development
### Run development server and show demo

```
npm run demo
```

### Run linter

```
npm run lint
```

### Fix linter errors

```
npm run fix
```

### Run tests

```
npm run test
```

### Build for production

```
npm run build
```