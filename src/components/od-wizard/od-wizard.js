const template = document.createElement( "template" );
template.innerHTML = `
     <style>

        :host {
            display: inline-block;
            width: 100%;
            border: 1px solid black;
        }

        :host([last="false"]) #finish-button {
            display: none;
        }

        :host([last="true"]) #next-button {
            display: none;
        }
        
        :host([first="true"]) #prev-button {
            display: none;
        }

        :host([first="true"]) #buttons-container {
            justify-content: flex-end;
        }

        #nav-container {
            display: flex;
        }
        
        #nav {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            width: 30%;
            background-color: darkgrey;
            border-right: 1px solid black;
        }

        #nav > label {
            display: flex;
            justify-content: center;
            align-items: center;

            color: dimgrey;

            width: 100%;
            height: 100%;
        }

        #nav > label[current='true'] {
            cursor: pointer;
            background: dimgrey;
            color: white;
        }

        #nav > label[enabled='true']:hover {
            cursor: pointer;
            background: dimgrey;
            color: white;
        }

        #nav > label[enabled='true'] {
            color: black;
        }

        #title-slot {            
            display: flex;
            justify-content: center;
            border-bottom: 1px solid black;
        }

        #pages-container {
            width: 100%;
        }

        #pages-slot::slotted(*[hide='true']) {
            display: none;
        }

        #buttons-container {
            display: flex;
            justify-content: space-between;
        }

    </style>
    
    <slot name='title' id="title-slot" part='title-slot'></slot>
    <div id='nav-container' part='nav-container'>
        <div id='nav' part='nav'></div>
        <div id='pages-container' part='pages-container'>
            <slot id="pages-slot"></slot>
            <div id='buttons-container' part='buttons-container'>
                <button id='prev-button' part='prev-button'>Back</button>
                <button id='next-button' part='next-button'>Next</button>
                <button id='finish-button' part='finish-button'>Finish</button>
            </div>
        </div>
    </div>
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "od-wizard" );

export class OdWizard extends HTMLElement {
    constructor() {
        super();
        //constructor creates the shadowRoot
        window.ShadyCSS && window.ShadyCSS.styleElement( this );
        if ( !this.shadowRoot ) {
            this.attachShadow( { mode: "open" } );
            this.shadowRoot.appendChild( template.content.cloneNode( true ) );
        }
    }

    connectedCallback() {        
        this._doneCallback = () => {};

        let config = { attributes: false, childList: true, subtree: true };
        this._observer = new MutationObserver( ( mutationsList, observer ) => { 
            this._mutationObserverCallback( mutationsList, observer ) 
        } );
        this._observer.observe( this.shadowRoot.querySelector( '#pages-slot' ), config );

        this.shadowRoot.querySelector( '#prev-button' ).addEventListener( 'click', ( e ) => {
            this._prevPage();
        } );
        this.shadowRoot.querySelector( '#next-button' ).addEventListener( 'click', ( e ) => {
            this._nextPage();
        } );
        this.shadowRoot.querySelector( '#finish-button' ).addEventListener( 'click', ( e ) => {
            this._finish();
        } );

        this._init();        
    }

    disconnectedCallback() {
        this._observer.disconnect();
    }

    async _init() {
        this._pages = await this._awaitPages();
        if( !this._pages || !this._pages.length || this._pages.length < 0 ) {
            return;
        }
        this._pageTitles = this._getPageTitles();
        this._buildNav();
        this._navigateToPage( this._pageTitles[0] );
    }

    _upgradeProperty( prop ) {
        if ( Object.prototype.hasOwnProperty.call( this, prop ) ) {
            var value = this[prop];
            delete this[prop];
            this[prop] = value;
        }
    }

    static get observedAttributes() {}

    attributeChangedCallback( attrName, oldValue, newValue ) {}

    _mutationObserverCallback( mutationsList, observer ) {
        this.dispatchEvent(
            new CustomEvent( 'od-wizard-mutation', { 
                bubbles: true, 
                detail: { 
                    'mutationsList': mutationsList,
                    'observer': observer
                }
            } )
        );
    }

    async _mutationPromise() {
        return new Promise( ( resolve ) => { 
            this.addEventListener( 'od-wizard-mutation', ( e ) => { this._mutationPromiseCallback( e, resolve ); } );
        } );
    }

    _mutationPromiseCallback( e, resolve ) {
        e.stopPropagation();
        this.removeEventListener( 'od-wizard-mutation', ( e ) => { this._mutationPromiseCallback( e, resolve ); } );
        resolve( e.detail ); 
    }

    async _awaitPages() {
        let pages = this._getPages();
        while( !pages || !pages.length || pages.length < 1 ) {
            await this._mutationPromise();
            pages = this._getPages();
        }
        return pages;
    }

    _getPages() {
        return this.shadowRoot.querySelector( '#pages-slot' ).assignedNodes()
                                                                .filter( ( node ) => {
                                                                    return ( node.nodeType === Node.ELEMENT_NODE && node.tagName === "OD-WIZARD-PAGE" )
                                                                });
    }

    _getPageTitles() {
        return this._pages.map( ( p ) => {
            return p.getAttribute( 'title' );
        } );
    }

    _buildNav() {
        let navCont = this.shadowRoot.querySelector( '#nav' );
        this._pageTitles.forEach( (t) => {
            let label = document.createElement( 'label' );
            label.innerHTML = t;
            label.setAttribute( 'part', 'nav-item' );
            label.addEventListener( 'click', (e) =>  {
                if( e.target.getAttribute( 'enabled' ) === 'true' ) {
                    this._navigateToPage( e.target.innerHTML );
                }
            } );
            navCont.appendChild( label );
        } );
        this._navItems = navCont.querySelectorAll( 'label' );
        this._navItems[0].setAttribute( 'enabled', true );
    }

    _navigateToPage( pageTitle ) {
        this._pages.forEach( (p) => {           
            if( p.getAttribute( 'title' ) === pageTitle ) {
                p.setAttribute( 'hide', false );
                this._currentPage = p;
                this._setNav( pageTitle );
                if( p === this._pages[0] ) {
                    this.setAttribute( 'first', true );
                } else {
                    this.setAttribute( 'first', false );                    
                }
                if( p === this._pages[this._pages.length - 1] ) {
                    this.setAttribute( 'last', true );
                } else {
                    this.setAttribute( 'last', false );                    
                }
                p.executeLoad();
            } else {
                p.setAttribute( 'hide', true );
            }
        } );
    }

    _setNav( pageTitle ) {
        for( let i = 0; i < this._navItems.length; i++ ) {
            let item = this._navItems[i];
            if( item.innerHTML === pageTitle ) {
                item.setAttribute( 'current', true );
            } else {
                item.setAttribute( 'current', false );
            }            
            this._setPart( item );
        }
    }

    _enableNavItem( pageTitle ) {
        for( let i = 0; i < this._navItems.length; i++ ) {
            let item = this._navItems[i];
            if( item.innerHTML === pageTitle ) {
                item.setAttribute( 'enabled', true );
            }
            this._setPart( item );
        }
    }

    _setPart( navItem ) {
        if( navItem.getAttribute( 'enabled' ) === 'true' && navItem.getAttribute( 'current' ) === 'true' ) {
            navItem.setAttribute( 'part', 'nav-item nav-item-enabled nav-item-current' );
        } else if( navItem.getAttribute( 'enabled' ) === 'true' ) {
            navItem.setAttribute( 'part', 'nav-item nav-item-enabled' );
        } else if( navItem.getAttribute( 'current' ) === 'true' ) {
            navItem.setAttribute( 'part', 'nav-item nav-item-current' );
        } else {
            navItem.setAttribute( 'part', 'nav-item' );
        }
    }

    _prevPage() {
        let index = this._pages.indexOf( this._currentPage );
        if( index !== 0 ) {
            this._navigateToPage( this._pages[index - 1].getAttribute( 'title' ) );
        }
    }

    _nextPage() {
        if( !this._currentPage.executeDone() ) {
            return;
        }
        let index = this._pages.indexOf( this._currentPage );
        if( index !== this._pages.length - 1 ) {
            let pageTitle = this._pages[index + 1].getAttribute( 'title' );
            this._enableNavItem( pageTitle );
            this._navigateToPage( pageTitle );
        }
    }

    _finish() {
        if( !this._currentPage.executeDone() ) {
            return;
        }
        this.doneCallback( this );
    }

    //properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    get doneCallback() {
        return this._doneCallback;
    }

    set doneCallback( callback ) {
        this._doneCallback = callback;
    }
}
