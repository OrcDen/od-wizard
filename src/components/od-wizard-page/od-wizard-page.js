const template = document.createElement( "template" );
template.innerHTML = `
    <style>

        :host {
            width: 100%;
        }

    </style>

    <slot id='page-content'></slot>
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "od-wizard-page" );

export class OdWizardPage extends HTMLElement {
    constructor() {
        super();
        //constructor creates the shadowRoot
        window.ShadyCSS && window.ShadyCSS.styleElement( this );
        if ( !this.shadowRoot ) {
            this.attachShadow( { mode: "open" } );
            this.shadowRoot.appendChild( template.content.cloneNode( true ) );
        }
    }

    connectedCallback() {
        this._loadCallback = () => {};
        this._doneCallback = () => { return true; };
    }

    disconnectedCallback() {}

    static get observedAttributes() {}

    attributeChangedCallback( attrName, oldValue, newValue ) {}

    get loadCallback() {
        return this._loadCallback;
    }

    set loadCallback( callback ) {
        this._loadCallback = callback;
    }

    get doneCallback() {
        return this._doneCallback;
    }

    set doneCallback( callback ) {
        this._doneCallback = callback;
    }

    executeLoad() {
        return this.loadCallback( this );
    }

    executeDone() {
        return this.doneCallback( this );
    }
}
